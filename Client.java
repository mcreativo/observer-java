public class Client
{
    public static void main(String[] args)
    {
        ClassA classA = new ClassA();
        ClassB classB = new ClassB();
        ClassC classC = new ClassC();

        classA.setClassB(classB);
        classA.setClassC(classC);

        classA.changedState();
    }
}
