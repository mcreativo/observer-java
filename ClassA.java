public class ClassA
{
    protected ClassB classB;

    protected ClassC classC;

    public void changedState()
    {
        System.out.println("ClassA is changing state");
        classB.changedStateOfClassA();
        classC.changedStateOfClassA();
    }

    public void setClassB(ClassB classB)
    {
        this.classB = classB;
    }

    public void setClassC(ClassC classC)
    {
        this.classC = classC;
    }
}
